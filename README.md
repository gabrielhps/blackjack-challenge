# blackjack-challenge

### Building & Running

To build and run this project it's necessary to have installed [Maven](https://maven.apache.org/install.html) and [JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html) (>=1.8).

* To build the project run the following command:
	```
	mvn clean package
	```
* To execute run the following command:
	```
	java -jar target/backjack-challenge
	```