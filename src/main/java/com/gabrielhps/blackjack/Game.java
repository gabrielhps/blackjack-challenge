package com.gabrielhps.blackjack;

import com.gabrielhps.blackjack.exception.BustHandException;
import com.gabrielhps.blackjack.exception.InvalidSplintException;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {

    private Deck deck;
    private Player player;
    private Player dealer;
    private int betAmount;
    private boolean isToContinueRunning;
    private boolean isToSkipDealerTurn;
    private boolean isSurrender;
    private boolean isPlayerTurnActive;
    private Scanner scanner;

    public Game() {
        this.deck = new Deck();
        this.player = new Player(100);
        this.dealer = new Player();
        this.betAmount = 10;
        this.isToContinueRunning = true;
        this.isToSkipDealerTurn = false;
        this.isSurrender = false;
        this.isPlayerTurnActive = true;
        this.scanner = new Scanner(System.in);
    }

    public void start() {
        System.out.println("=== WELCOME TO BLACKJACK ===");

        while (isToContinueRunning) {
            dealInitialCards();

            playerTurn();
            dealerTurn();
            computeResult();

            askToPlayAgain();
            reset();
        }

        scanner.close();
    }

    private void dealInitialCards() {
        player.takeCard(deck.pop());
        player.takeCard(deck.pop());
        dealer.takeCard(deck.pop());
        dealer.takeCard(deck.pop());

        System.out.println();
        System.out.println();
        System.out.println("Player's hand: " + player.getHand());
        System.out.println("Dealer's hand: " + dealer.getHand().toStringFirstCard());
        System.out.println();
        System.out.println();
    }

    private void playerTurn() {
        System.out.println("=== PLAYER TURN ===");

        if (playerHas21()) {
            System.out.println();
            System.out.println("Player already has 21!");
            System.out.println();
            return;
        }

        while (isPlayerTurnActive) {
            switch (askPlayerChoice()) {
                case 1:
                    hit();
                    break;
                case 2:
                    stand();
                    break;
                case 3:
                    doubleDown();
                    break;
                case 4:
                    split();
                    break;
                case 5:
                    surrender();
                    break;
            }
        }
    }

    private boolean playerHas21() {
        return player.getHand().getValue() == 21;
    }

    private void hit() {
        try {
            player.takeCard(deck.pop());
            isPlayerTurnActive = !playerHas21();
            System.out.println("Player's hand: " + player.getHand());
            System.out.println();
        } catch (BustHandException e) {
            System.out.println(e.getMessage());
            isToSkipDealerTurn = true;
            isPlayerTurnActive = false;
        }
    }

    private void stand() {
        isPlayerTurnActive = false;
    }

    private void surrender() {
        betAmount /= 2;
        isToSkipDealerTurn = true;
        isSurrender = true;
        isPlayerTurnActive = false;
    }

    private void doubleDown() {
        try {
            betAmount *= 2;
            player.takeCard(deck.pop());
            isPlayerTurnActive = false;
            System.out.println("Player's hand: " + player.getHand());
            System.out.println();
        } catch (BustHandException e) {
            System.out.println(e.getMessage());
            isToSkipDealerTurn = true;
            isPlayerTurnActive = false;
        }
    }

    // this operation is incomplete and not 100% functional
    private void split() {
        try {
            player.splitHand();
            player.getHands().forEach(h -> h.addCard(deck.pop()));
            betAmount *= 2;
            System.out.println("Player's hand: " + player.getHands());
            System.out.println();
        } catch (InvalidSplintException e) {
            System.out.println(e.getMessage());
            System.out.println();
        }
    }

    private void dealerTurn() {
        if (isToSkipDealerTurn)
            return;

        System.out.println("=== DEALER TURN ===");
        System.out.println();
        System.out.println("Dealer's hand: " + dealer.getHand());

        while (dealer.getHand().getValue() < 18) {
            try {
                dealer.takeCard(deck.pop());
                System.out.println("Dealer's hand: " + dealer.getHand());
            } catch (BustHandException e) {
                System.out.println(e.getMessage());
                break;
            }
        }
    }

    private void computeResult() {
        System.out.println();
        System.out.println("==== RESULT ====");
        System.out.println();
        System.out.println("Player's hand: " + player.getHand());
        System.out.println("Dealer's hand: " + dealer.getHand());
        System.out.println();

        if (isSurrender) {
            System.out.println("PLAYER HAS SURRENDER");
            player.payBet(betAmount);
        } else if (player.getHand().ties(dealer.getHand())) {
            System.out.println("TIED");
        } else if (player.getHand().wins(dealer.getHand())) {
            System.out.println("PLAYER WINS");
            player.collectWinnings(betAmount);
        } else {
            System.out.println("PLAYER LOSES");
            player.payBet(betAmount);
        }

        System.out.println("Player's amount of chips: " + player.getAmountOfChips());
    }

    private void askToPlayAgain() {
        while (true) {
            System.out.print("Would you like to play again? (Y/N): ");
            String playerChoice = scanner.next();

            if (!"Y".equalsIgnoreCase(playerChoice) && !"N".equalsIgnoreCase(playerChoice)) {
                System.out.println("Invalid option. Please, try again");
            } else {
                isToContinueRunning = "Y".equalsIgnoreCase(playerChoice);
                break;
            }
        }
    }

    private int askPlayerChoice() {
        while (true) {
            try {
                System.out.println("What would you like to do: ");
                System.out.println("(1) - Hit");
                System.out.println("(2) - Stand");
                System.out.println("(3) - Double Down");
                System.out.println("(4) - Split");
                System.out.println("(5) - Surrender");
                System.out.print("Please, choose an option: ");

                int playerChoice = scanner.nextInt();

                if (playerChoice < 1 || playerChoice > 5)
                    System.out.println("Invalid option. Please try again.");
                else {
                    System.out.println();
                    return playerChoice;
                }
            } catch (InputMismatchException e) {
                scanner.next();
                System.out.println("Invalid option. Please try again.");
            }
        }
    }

    private void reset() {
        deck.reset();
        player.clearHand();
        dealer.clearHand();
        betAmount = 10;
        isToSkipDealerTurn = false;
        isSurrender = false;
        isPlayerTurnActive = true;
    }

}
