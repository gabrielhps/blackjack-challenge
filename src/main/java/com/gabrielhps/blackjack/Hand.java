package com.gabrielhps.blackjack;

import com.gabrielhps.blackjack.exception.BustHandException;
import com.gabrielhps.blackjack.exception.InvalidSplintException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hand {

    private int aces;
    private List<Card> cards;
    

    public Hand() {
        this.aces = 0;
        this.cards = new ArrayList<>();
    }

    public Hand(Card... cards) {
        for (Card c : cards)
            addCard(c);
    }

    public void addCard(Card card) {
        cards.add(card);

        if (Rank.ACE.equals(card.getRank()))
            aces += 1;

        if (getValue() > 21)
            throw new BustHandException(this);
    }

    public List<Hand> split() {
        if (cards.size() != 2 || !cards.get(0).getRank().equals(cards.get(1).getRank()))
            throw new InvalidSplintException();

        return cards.stream().map(Hand::new).collect(Collectors.toList());
    }

    public boolean wins(Hand otherHand) {
        int handValue = getValue();
        int otherHandValue = otherHand.getValue();

        if (handValue > 21)
            return false;

        if (otherHandValue > 21)
            return true;

        return handValue > otherHandValue;
    }

    public boolean ties(Hand otherHand) {
        int handValue = getValue();
        int otherHandValue = otherHand.getValue();

        return (handValue > 21 && otherHandValue > 21) || handValue == otherHandValue;
    }

    public int getValue() {
        int total = cards.stream().map(Card::getValue).reduce(0, (x, y) -> x + y);
        int tmp_aces = aces;

        while (total > 21 && tmp_aces != 0) {
            total -= 10;
            tmp_aces -= 1;
        }

        return total;
    }

    @Override
    public String toString() {
        return String.format("Cards: %s - Total: %d", cards, getValue());
    }

    public String toStringFirstCard() {
        if (cards.size() == 0)
            return "Card: None - Total: 0";

        Card card = cards.get(0);
        return String.format("Card: %s - Total: %d", card, card.getValue());
    }

}
