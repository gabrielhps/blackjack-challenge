package com.gabrielhps.blackjack;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Card {

    private static final Map<Rank, Integer> VALUE_BY_RANK;

    private final Suit suit;
    private final Rank rank;

    static {
        VALUE_BY_RANK = new HashMap<>();
        VALUE_BY_RANK.put(Rank.TWO, 2);
        VALUE_BY_RANK.put(Rank.THREE, 3);
        VALUE_BY_RANK.put(Rank.FOUR, 4);
        VALUE_BY_RANK.put(Rank.FIVE, 5);
        VALUE_BY_RANK.put(Rank.SIX, 6);
        VALUE_BY_RANK.put(Rank.SEVEN, 7);
        VALUE_BY_RANK.put(Rank.EIGHT, 8);
        VALUE_BY_RANK.put(Rank.NINE, 9);
        VALUE_BY_RANK.put(Rank.TEN, 10);
        VALUE_BY_RANK.put(Rank.JACK, 10);
        VALUE_BY_RANK.put(Rank.QUEEN, 10);
        VALUE_BY_RANK.put(Rank.KING, 10);
        VALUE_BY_RANK.put(Rank.ACE, 11);
    }

    public Card(Rank rank, Suit suit) {
        this.suit = suit;
        this.rank = rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public Rank getRank() {
        return rank;
    }

    public int getValue() {
        return VALUE_BY_RANK.get(rank);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Card card = (Card) o;

        return suit == card.suit && rank == card.rank;
    }

    @Override
    public int hashCode() {
        return Objects.hash(suit, rank);
    }

    @Override
    public String toString() {
        return String.format("%s %s", rank.getSymbol(), suit);
    }

}
