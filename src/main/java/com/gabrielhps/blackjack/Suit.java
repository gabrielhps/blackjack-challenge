package com.gabrielhps.blackjack;

public enum Suit {

    HEARTS,
    CLUBS,
    DIAMONDS,
    SPADES

}
