package com.gabrielhps.blackjack;

import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;

public class Deck {

    private Deque<Card> cards;

    public Deck() {
        this.cards = initializeCards();
    }

    public Card pop() {
        return cards.pop();
    }

    public void reset() {
        cards = initializeCards();
    }

    private LinkedList<Card> initializeCards() {
        LinkedList<Card> cards = new LinkedList<>();

        for (Rank rank : Rank.values())
            for (Suit suit : Suit.values())
                cards.add(new Card(rank, suit));

        Collections.shuffle(cards);

        return cards;
    }

}
