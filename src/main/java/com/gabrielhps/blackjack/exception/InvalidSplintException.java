package com.gabrielhps.blackjack.exception;

public class InvalidSplintException extends RuntimeException {

    public InvalidSplintException() {
        super("It is not possible to split this hand");
    }

}
