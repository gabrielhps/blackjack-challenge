package com.gabrielhps.blackjack.exception;

import com.gabrielhps.blackjack.Hand;

public class BustHandException extends RuntimeException {

    public BustHandException(Hand hand) {
        super("Bust! " + hand);
    }

}
