package com.gabrielhps.blackjack;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private int amountOfChips;
    private List<Hand> hands;

    public Player(int initialAmountOfChips) {
        this.amountOfChips = initialAmountOfChips;
        this.hands = new ArrayList<>();
        this.hands.add(new Hand());
    }

    public Player() {
        this(1_000_000);
    }

    public Hand getHand() {
        return hands.get(0);
    }

    public List<Hand> getHands() {
        return hands;
    }

    public int getAmountOfChips() {
        return amountOfChips;
    }

    public void payBet(int amount) {
        amountOfChips -= amount;
    }

    public void collectWinnings(int amount) {
        amountOfChips += amount;
    }

    public void takeCard(Card card) {
        hands.get(0).addCard(card);
    }

    public void splitHand() {
        hands = hands.get(0).split();
    }

    public void clearHand() {
        hands.clear();
        hands.add(new Hand());
    }

}
